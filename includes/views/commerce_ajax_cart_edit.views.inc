<?php

/*
 * Implements hook_views_data_alter().
 */
function commerce_ajax_cart_edit_views_data_alter(&$data) {
  if (isset($data['commerce_line_item'])) {
    $data['commerce_line_item']['cart_line_item_ajax_edit'] = array(
      'field' => array(
        'title' => t('Ajax Cart Line Item Edit link'),
        'help' => t('Link to provide ajax powered edit form for a cart line item.'),
        'handler' => 'commerce_ajax_cart_edit_handler_field_cart_line_item_ajax_edit',
      ),
    );
  }
}
