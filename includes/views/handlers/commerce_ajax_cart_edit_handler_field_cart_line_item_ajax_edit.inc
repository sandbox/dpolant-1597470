<?php

class commerce_ajax_cart_edit_handler_field_cart_line_item_ajax_edit extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['line_item_id'] = 'line_item_id';
    
    // Set real_field in order to make it generate a field_alias.
    $this->real_field = 'line_item_id';    
  }
  
  function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }
  
  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);
    $options['show_quantity'] = array('default' => FALSE);
    $options['target_selector'] = array('default' => '');
    $options['add_row_wrapper'] = array('default' => TRUE);
    $options['effect'] = array('default' => 'slide');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );

    $form['show_quantity'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display a textfield quantity widget on the edit form.'),
      '#default_value' => $this->options['show_quantity'],
    );
    
    $form['add_row_wrapper'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add wrapping html to line item edit rows.'),
      '#description' => t('Wrap views rows in html based on the style plugin. One reason to leave this off
        would be if you are using a table style but do not want the edit form wrapped in a <tr> because you 
        are adding the element outside of the <table> boundaries.'),
      '#default_value' => $this->options['add_row_wrapper'],
    );
    
    $form['effect'] = array(
      '#type' => 'radios',
      '#title' => t('Ajax effect'),
      '#description' => t('Effect to employ when form appears'),
      '#default_value' => $this->options['effect'],   
      '#options' => array(
        'none' => t('None'),
        'fade' => t('Fade'),
        'slide' => t('Slide')
      )
    );
    
    $form['target_selector'] = array(
      '#type' => 'textfield',
      '#title' => t('jQuery selector'),
      '#description' => t('jQuery selector target for edit form element. You may use views token replacements for this value.'),
      '#default_value' => $this->options['target_selector'],       
    );
  }
  
  function views_form(&$form, &$form_state) {    
    $form['#attached']['js'][] = drupal_get_path('module', 'commerce_ajax_cart_edit') . '/commerce_ajax_cart_edit.js';
    $field_name = $this->options['id'];        
    
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $form[$field_name] = array(
      '#tree' => TRUE,
    );
    
    $style_plugin = $this->view->style_plugin;    
    
    // Store view details in form state so that it can be rebuilt if necessary
    $form_state['view_details'] = array(
      'name' => $this->view->name,
      'display' => $this->view->current_display,
      'style' => $style_plugin->definition['name'],
      'row_wrap' => $this->options['add_row_wrapper']
    );
    
    $show_quantity = $this->options['show_quantity'];
    
    // At this point, the query has already been run, so we can access the results
    // in order to get the base key value (for example, nid for nodes).
    foreach ($this->view->result as $row_id => $row) {
      $line_item_id = $this->get_value($row);
      
      // Tokenize the target selector value
      $selector = $style_plugin->tokenize_value($this->options['target_selector'], $row_id);
      
      $form[$field_name][$row_id] = array(
        '#type' => 'submit',
        '#value' => t('Edit'),
        '#name' => 'edit-line-item-' . $row_id,
        '#attributes' => array('class' => array('edit-line-item')),
        '#line_item_id' => $line_item_id,        
        '#row_id' => $row_id,
        '#row_selector' => $selector,
        '#field_name' => $field_name,
        '#ajax' => array(
          'callback' => 'commerce_ajax_cart_edit_line_item_edit_js',          
        ),
        '#submit' => array('commerce_ajax_cart_edit_line_item_edit_submit'),
      );
      
      if ($this->options['effect'] != 'none') {
        $form[$field_name][$row_id]['#ajax']['effect'] = $this->options['effect'];
        $form_state['effect'] = $this->options['effect'];
      }
      
      // Build the editing form if the row has been selected for editing, or if 
      // cart refresh callback is detected.
      if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'edit-line-item-' . $row_id) {        
        $line_item = commerce_line_item_load($line_item_id);
        $form_state['line_item'] = $line_item;
        commerce_ajax_cart_edit_attach_line_item_edit_form($line_item, $form, $form_state, $this, $show_quantity);
      }
    }
    
    // Also rebuild line item edit form if the triggering element is an attribute
    // selector widget.
    if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#ajax']['callback'] == 'commerce_cart_add_to_cart_form_attributes_refresh') {
      if (!empty($form_state['line_item'])) {
        commerce_ajax_cart_edit_attach_line_item_edit_form($form_state['line_item'], $form, $form_state, $this, $show_quantity);
      }
      $line_item_id = $form_state['line_item']->line_item_id;
    }
  }
}
