(function ($) {
  // Custom fade out function
  $.fn.fadeRemove = function(restripeSelector) {
    $(this).fadeOut('fast', function() {      
      $(this).remove();
      
      if (typeof restripeSelector != 'undefined') {
        $(restripeSelector).restripe();
      }
    });
  }
  
  // Custom slide up function
  $.fn.slideRemove = function(restripeSelector) {
    $(this).slide('fast', function() {
      $(this).remove();
      
      if (typeof restripeSelector != 'undefined') {
        $(restripeSelector).restripe();
      }
    });
  }
  
  // Direct restripe function
  $.fn.restripe = function() {
    $('> tbody > tr:visible, > tr:visible', $(this))
      .removeClass('odd even')
      .filter(':even').addClass('odd').end()
      .filter(':odd').addClass('even');
  }  
})(jQuery);
